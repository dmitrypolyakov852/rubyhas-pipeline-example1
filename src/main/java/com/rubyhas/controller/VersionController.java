package com.rubyhas.controller;

import java.util.concurrent.TimeUnit;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/api")
public class VersionController {

    @Get(uri = "/sleep/{sec}")
    public HttpResponse<?> timeOut1(int sec) throws InterruptedException {
        TimeUnit.SECONDS.sleep(sec);
        return HttpResponse.ok();
    }

    @Get(uri = "/version")
    public HttpResponse<String> getVersion() {
        return HttpResponse.ok("Current version: 1.0.0");
    }
}

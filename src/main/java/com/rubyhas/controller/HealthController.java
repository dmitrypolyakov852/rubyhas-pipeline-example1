package com.rubyhas.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller
public class HealthController {

    @Get(uri = "/health")
    public HttpResponse<?> health() {
        return HttpResponse.ok();
    }
}

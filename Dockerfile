FROM amazoncorretto:11

COPY build/libs/*-all.jar app.jar

CMD java -jar app.jar ${JAVA_OPTS}
